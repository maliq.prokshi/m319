import java.util.Scanner;

public class GgtRechnerC {

	public static void main(String[] args) {
		//include scanner for reading input
		Scanner scan = new Scanner(System.in);
		
		int p1, p2;  //our variables
		p1 = 0;
		p2 = 0;
		System.out.println("Eingabe zweier Zahlen:");
		p1 = scan.nextInt();
		p2 = scan.nextInt();
		
		while((p1 - p2) != 0) {
			
			if((p1 - p2) > 0) {
				p1 = (p1 - p2);
			}
			else {
				p2 = (p2 - p1);
			}
			
		}

		System.out.println("Der groesste gemeinsamer Teiler ist " + p1);
		
		scan.close(); //prevents resource leak
	}

}
