![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

# Kompetenznachweise A4 D4 I4


## Ein Projekt (P2) in Einzelarbeit umsetzen.

Die Kompetenfelder A4 D4 I4 müssen in einem Projekt umgesetzt werden. Das Projekt muss inhaltlich über dem Niveau 3 liegen und folgende Anforderungen erfüllen.

| KN | Kompetenz | Anforderung |
|:--:|-----------|-------------|
| A4| Eine eigene Anforderungsliste formulieren. | [**Liste mit funktionalen Anforderungen**](../../N2-Task_Description), dazu [**Aktivitätsdiagramm**](../../N3-Complex_AD) (oder [UML-UseCase Diagramm](../../N4-UseCase_UML)).|
|D4| [**Komplexe Datentypen**](../../N3-Complex_Datatypes) deklarieren und initialisieren. <br> [**Funktionen mit Parameterübergabe**](../../N4-Call_by).| Die nötigen Prozessdaten sind in globalen Variablen (Klassen-/Instanzvariablen) abgelegt, klar bezeichnet und der erwartete Inhalt beschrieben. (Variablenliste) <br> Die Funktionen verwenden Parameter zur Datenübergabe: primitive und komplexe Datentyp |
|I4| Anfordrungsliste ist programmtechnisch umgesetzt. Der Quellcode entspricht der [**TBZ-Codekonvention**](../../N3-Code_Formatting). | Das Programm verwendet Standardmethoden zur Bearbeitung der komplexen Datentypen (D4). <br> Das Programm widerspiegelt die Anforderungen und demonstriert fundiertes Wissen wie Probleme in Strukturen unterteilt (mit Methoden) werden. |



## Vorgehen:

1. Platzieren Sie die *kopierte* Planer-Karte vom KN-Feld A4 in ihren Planer "**In progress**".
2. Erstellen Sie eine **Anforderungsliste (P2) *IN* der Planer-Karte** und besprechen Sie mit der Lehrperson ihr Vorhaben anhand ihrer Anforderungsliste. Das Programm muss Niveau-gerecht sein. ![Planercard](../x_gitressourcen/Planer Card Own Task.png)
3. **Stellen** Sie ihr Projekt der **Lehrperson vor** zu Sicherstellung des geforderten Niveaus 4. <br> -
4. Erstellen Sie ein **Aktivitätsdiagramm (oder ein Use Case-Diagramm)** dazu (--> Portfolio A4: P2) <br> -
5. Wenn möglich planen Sie **komplexe Datentypen** (mind. 2 Typen) im Programm ein. (D4) 
6. Entwerfen Sie eine **Ablaufstruktur (EVA) mit Funktionen**; wenn möglich mit Parameterübergabe. (D4) <br> -
7. Setzen Sie ihre Anforderungsliste in *einem* Java-File um. (Einzelarbeit) <br> -
8. Platzieren Sie die entsprechende Planer-Karte im Planer auf "**To examine**". <br> Ein Link zum Programmquellcode kann in der Planerkarte gesetzt werden.
9. Informieren Sie die LP und demonstrieren Sie ihr Lösungs-Programm.(Abgabe A4, D4, I4)
10. Die Lehrperson platziert die entsprechende Planer-Karte im Planer auf "**Passed**" *oder* "**Redo**" (--> 3) *oder* gibt ihnen entsprechend Teilpunkt.

### Bewertung:

Pro KN-Feld 1 Punkte, d.h. max. 3 Punkte! <br>
Sie müssen das Urheberrecht auf ihre Abgaben besitzen: Teilaufgaben ohne Urheberrecht werden nicht bewertet. <br>
Unfertige Teilaufgaben werden entsprechend der Leistung prozentual teilbewertet.












